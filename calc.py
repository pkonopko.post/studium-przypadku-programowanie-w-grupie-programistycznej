def add(x, y):
    return x + y

def subtract(x, y):
    return x - y

def multiply(x, y):
    return x * y

def divide(x, y):
    if y != 0:
        return x / y
    else:
        return "Nie można dzielić przez zero!"

def sqrt(x):
    if x >= 0:
        return math.sqrt(x)
    else:
        return "Nie można obliczyć pierwiastka z liczby ujemnej!"

def power(x, y):
    return x ** y

def percentage(x, y):
    return (x * y) / 100

def calculator():
    print("\n========================")
    print("   Prosty Kalkulator")
    print("========================\n")
    print("Wybierz operację:")
    print("1. Dodawanie")
    print("2. Odejmowanie")
    print("3. Mnożenie")
    print("4. Dzielenie")
    print("5. Pierwiastkowanie")
    print("6. Potęgowanie")
    print("7. Obliczanie procentu")
    print("q. Wyjdź z kalkulatora\n")

    while True:
        choice = input("Wpisz numer operacji (1/2/3/4/5/6/7) lub 'q' aby zakończyć: ")

        if choice == 'q':
            print("Dziękujemy za skorzystanie z kalkulatora! Do widzenia!")
            break

        if choice in ['1', '2', '3', '4', '5', '6', '7']:
            try:
                if choice == '5':
                    num = float(input("Wprowadź liczbę do spierwiastkowania: "))
                    result = sqrt(num)
                    print(f"\nWynik: √{num} = {result}\n")
                elif choice == '6':
                    base = float(input("Wprowadź podstawę: "))
                    exponent = float(input("Wprowadź wykładnik: "))
                    result = power(base, exponent)
                    print(f"Wynik: {base} ^ {exponent} = {result}")
                elif choice == '7':
                    total = float(input("Wprowadź całość: "))
                    percentage_value = float(input("Wprowadź procent: "))
                    result = percentage(total, percentage_value)
                    print(f"Wynik: {percentage_value}% z {total} = {result}")
                else:
                    num1 = float(input("Wprowadź pierwszą liczbę: "))
                    num2 = float(input("Wprowadź drugą liczbę: "))

                    if choice == '1':
                        print(f"\nWynik: {num1} + {num2} = {add(num1, num2)}\n")
                    elif choice == '2':
                        print(f"\nWynik: {num1} - {num2} = {subtract(num1, num2)}\n")
                    elif choice == '3':
                        print(f"\nWynik: {num1} * {num2} = {multiply(num1, num2)}\n")
                    elif choice == '4':
                        result = divide(num1, num2)
                        print(f"\nWynik: {num1} / {num2} = {result}\n")
            except ValueError:
                print("\nBłąd: Wprowadź prawidłową liczbę.\n")
        else:
            print("\nNieprawidłowy wybór. Spróbuj ponownie.\n")

if __name__ == "__main__":
    calculator()
